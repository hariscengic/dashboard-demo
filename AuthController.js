/**
 * Created by hariscengic on 2/8/16.
 */
(function () {
    angular
        .module('app')
        .controller('LoginController', [
            '$mdDialog', '$interval', '$scope', '$http', '$mdToast', '$timeout', '$state',
            LoginController
        ])

    var vm = this;

    function LoginController($mdDialog, $interval, $scope, $http, $mdToast, $timeout, $state) {


        $scope.changeState = function () {
            $state.go('home.tools');
        }

        $scope.empty = false;

        $scope.username = ""
        $scope.password = ""

        $scope.spinner = false
        $scope.timeout = false
        $scope.incorrect = false
        $scope.permissions = false


        function randomHex(length) {
            var i, hex = '';
            for (i = 0; i < length; i++) {
                hex = hex + Math.floor(Math.random() * 16).toString(16);
            }
            return hex;
        }

        $scope.SignIn = function () {

            $scope.permissions = false
            $scope.incorrect = false

            $scope.spinner = true;
            $scope.timeout = true;
            $scope.deviceid = randomHex(16);

            console.log("login info: ")

            var username = $scope.username;
            console.log("username: " + username)

            var password = $scope.password;
            console.log("password: " + password)


            $http.post('**API ENDPOINT HIDDEN**', 'Password=' + $scope.password + "&" + "UserName=" + $scope.username, {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest'}
                }
            ).success(function (data, status, headers, config, response) {
                    console.log(response)
                    console.log("Successfully validated user")
                    //check that these generated types are true
                    return $http.put('**API ENDPOINT HIDDEN**', '<?xml version="1.0" encoding="utf-8"?><RedE xmlns="**API SCHEMA HIDDEN**">' +
                        '<SignInRequest><UserName>' + $scope.username + '</UserName>' +
                        '<PWD>' + btoa($scope.password) + '</PWD>' +
                        '<DeviceID>001</DeviceID>' +
                        '<DeviceToken>' + $scope.deviceid + '</DeviceToken>' +
                        '<OSVersion>47</OSVersion>' +
                        '<OSType>Chrome</OSType>' +
                        '<DeviceType>BROWSER</DeviceType>' +
                        '<PushPlatform>FAYE</PushPlatform>' +
                        '</SignInRequest></RedE>', {
                        headers: {'Content-Type': 'application/xml'}
                    })
                        .success(function (res) {
                            console.log('nested handler, user authenticated');
                            console.log(res);
                            $timeout(function () {
                                $scope.timeout = false;
                                $scope.spinner = false;
                                $scope.incorrect = false;
                            }, 1000);
                        });
                    $timeout(function () {
                        $scope.timeout = false;
                        $scope.spinner = false;
                        $scope.incorrect = false;
                    }, 1000);
                }).error(function (response, data, status, headers, config) {
                    console.log(response)
                    console.log("Cannot validate user")
                    $timeout(function () {
                        $scope.timeout = false;
                        $scope.spinner = false;
                        $scope.incorrect = true;
                    }, 1500);
                });


            if (password == '' && username == '') {
                $scope.empty = true;
            }
        }


    }
})();

