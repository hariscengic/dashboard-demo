
(function () {
    angular
    .module('app')
        .controller('ToolsController', [
            '$mdDialog', '$interval', '$scope' , '$http' , '$mdToast', '$cookies', '$cookieStore',
            ToolsController
        ])

    var vm = this;

    function ToolsController($mdDialog, $interval, $scope, $http, $mdToast, $cookies, $cookieStore) {

        $scope.numFail = $cookies.numFail;
        $scope.numSuccess = $cookies.numSuccess;

        $scope.numGetUser = $cookies.numGetUser;
        $scope.numDeviceActivations = $cookies.numDeviceActivations;
        $scope.numChangePass = $cookies.numChangePass;
        $scope.numChangeEmail = $cookies.numChangePass;

        $scope.authUsername = $cookies.authUsername;
        $scope.authPassword = $cookies.authPassword;

        $scope.inputType = 'password';
        $scope.pass = 'Show Password';

        $scope.cbusername = true;
        $scope.cblastname = true;

        console.log(vm.loggedIn);
        console.log(vm.title);


        $scope.saveAuth = function(){
            $cookies.authUsername = $scope.authUsername;
            $cookies.authPassword = $scope.authPassword;
        };

        $scope.hideShowPassword = function(){
            if ($scope.inputType == 'password') {
                $scope.inputType = 'text'
                $scope.pass = 'Hide Password';
            }
            else {
                $scope.inputType = 'password';
                $scope.pass = 'Show Password';
            }
        };


        $scope.changePassOutput = "";
        $scope.username = "";
        $scope.password = "";
        $scope.password2 = "";

        $scope.copied = function () {
            showActionToast('Text Copied to Clipboard')
        }


        //CHANGE EMAIL
        $scope.changeEmail = function () {


            $http.put('**API ENDPOINT HIDDEN**users/' + $scope.username, {Email: $scope.email}, {
                headers: {
                    "Authorization": 'Basic ' + btoa($scope.authUsername + ":" + $scope.authPassword)
                }
            ).success(function (data, status, headers, config, response) {
                    console.log(response)
                    showActionToast('Successfully Changed Email')
                    $cookies.numChangeEmail = $scope.numChangeEmail + 1;
                }).error(function (data, status, headers, config) {
                    showActionToast('Error Changing Email')
                });
        }


        //ACTIVATE USER
        $scope.activateUser = function () {

            $http.post('**API ENDPOINT HIDDEN**deviceactivations', {UserName: $scope.username}, {
                headers: {
                    "Authorization": 'Basic ' + btoa($scope.authUsername + ":" + $scope.authPassword)
                }
            ).success(function (data, status, headers, config, response) {
                    console.log(response)
                    showActionToast('Successfully Activated User')
                }).error(function (data, status, headers, config) {
                    showActionToast('Error Activating User')
                });

        }

        //SEND PASSWORD RESET EMAIL
        $scope.resetPassword = function () {
            console.log($scope.username)


            $http.post('**API ENDPOINT HIDDEN**forgottenpasswords', {RedeAppId: $scope.username}, {
                headers: {
                    "Authorization": 'Basic ' + btoa($scope.authUsername + ":" + $scope.authPassword)
                }
            ).success(function (data, status, headers, config, response) {
                    console.log(response)
                    showActionToast('Successfully Sent Password Reset Email for: ' + $scope.username)
                }).error(function (data, status, headers, config) {
                    showActionToast('Error No User With Username Found')
                });
        }


        //CHANGE PASS
        $scope.changePass = function () {

            $http.post('**API ENDPOINT HIDDEN**unlocks', {
                    RedeAppId: $scope.username,
                    NewPassword: $scope.newpass
                }, {
                headers: {
                    "Authorization": 'Basic ' + btoa($scope.authUsername + ":" + $scope.authPassword)
                }
            ).success(function (data, status, headers, config, response) {
                    console.log(response)
                    showActionToast('Successfully Changed Password')
                }).error(function (data, status, headers, config) {
                    showActionToast('Error Changing Password')
                });

        }

        $scope.username = '';

        $scope.myData = [];
        $scope.deviceslabel = '';
        $scope.query = ''

        //GET USER INFO
        $scope.getUser = function () {

            console.log($scope.authUsername + ":" + $scope.authPassword);

            $scope.deviceslabel = 'Devices (scroll to see all)'
            $scope.userNetworks = ' '
            $scope.temp = ' '
            $scope.networks = ' '
            var loc = '';
            $http.get('**API ENDPOINT HIDDEN**users/' + $scope.username, {
                headers: {
                    "Authorization": 'Basic ' + btoa($scope.authUsername + ":" + $scope.authPassword)
                }
            }).success(function (response, data) {
                showActionToast('Successfully Found User')
                console.log(response)
                $scope.myData = angular.fromJson(response);
                $cookies.numGetUser = parseInt($scope.numGetUser + 1);

                for (var j = 0; j < $scope.myData.Locations.length; j++) {

                    var networkName = $scope.myData.Locations[j].Name;
                    $scope.networks = networkName
                    $scope.temp = $scope.networks + ", " + $scope.temp
                }

                $scope.devices = [];

                var devicesOSType = [];
                var devicesOSVersion = [];
                var devicesEnabled = [];
                var devicesInactive = [];

                for (var i = 0; i < $scope.myData.Devices.length; i++) {
                    $scope.devices.push($scope.myData.Devices[i]);

                    if ($scope.myData.Devices[i].OSType != "TODO") {
                        devicesOSType.push($scope.myData.Devices[i].OSType);
                    }
                    if ($scope.myData.Devices[i].OSVersion != "TODO") {
                        devicesOSVersion.push($scope.myData.Devices[i].OSVersion);
                    }
                    devicesEnabled.push($scope.myData.Devices[i].Enabled);
                    devicesInactive.push($scope.myData.Devices[i].Inactive);
                }

                console.log($scope.devices)
                console.log(devicesOSType)
                console.log(devicesOSVersion)
                console.log(devicesEnabled)
                console.log(devicesInactive)

                loc = $scope.myData.Locations[1].LocationId;
                console.log($scope.locid);
                console.log(loc);



                $scope.userNetworks = $scope.temp;
                $scope.userNetworks = $scope.userNetworks.substring(0, $scope.userNetworks.length - 3);

                console.log($scope.userNetworks);


            }).error(function (response, error, data, status, headers, config) {
                $scope.statuscode = response.status;
                showActionToast('Error Finding User -- User Does Not Exist')
            });


    }


            $scope.getQuery = function () {

                if ($scope.cbusername)
                $scope.querylabel = 'All usernames with the selected query (scroll to see all)'
                $http.get('**API ENDPOINT HIDDEN**users?search=RedeAppId.Contains("' + $scope.query + '")', {
                    headers: {
                        "Authorization": 'Basic ' + btoa('redeops:testing')
                    }
                }).success(function (response, data) {
                    console.log(response)
                    $scope.search = angular.fromJson(response);
                    if ($scope.search.Count == 0){
                        showActionToast('No Found User(s)')
                    }
                    else {
                        showActionToast('Successfully Queried User(s)')


                        console.log($scope.search.Items[0].FirstName)

                        $scope.items = [];


                        for (var i = 0; i < $scope.search.Items.length; i++) {
                            $scope.items.push($scope.search.Items[i]);
                        }

                        console.log($scope.items)

                    }
                }).error(function (response, error, data, status, headers, config) {
                    $scope.statuscode = response.status;
                    showActionToast('Error Finding User(s) -- Cannot connect')
                });

                //search by last name
                $http.get('**API ENDPOINT HIDDEN**users?search=LastName.Contains("' + $scope.query + '")', {
                    headers: {
                        "Authorization": 'Basic ' + btoa('redeops:testing')
                    }
                }).success(function (response, data) {
                    console.log(response)
                    $scope.namesearch = angular.fromJson(response);

                    $scope.querynamelabel = 'All last names with the selected query (scroll to see all)'

                    if ($scope.namesearch.Count == 0){
                        showActionToast('No Found User(s)')
                    }
                    else {
                        showActionToast('Successfully Queried User(s)')


                        $scope.nameitems = [];


                        for (var i = 0; i < $scope.namesearch.Items.length; i++) {
                            $scope.nameitems.push($scope.namesearch.Items[i]);
                        }

                        console.log($scope.nameitems)


                    }
                }).error(function (response, error, data, status, headers, config) {
                    $scope.statuscode = response.status;
                    showActionToast('Error Finding User(s) -- Does Not Exist')
                });
            }

        $scope.clear = function () {
            $scope.myData = '';
            $scope.username = '';
            $scope.email = '';
            $scope.LastName = '';
            $scope.FirstName = '';
            $scope.newpass = '';
            $scope.newpass1 = '';
            $scope.devices = '';
            $scope.items = '';
            $scope.nameitems = '';
            $scope.querynamelabel = '';
            $scope.namelabel = '';
            $scope.deviceslabel = '';
            $scope.networks = '';
            $scope.userNetworks = '';
            $scope.temp = '';

            console.log("cleared")
        }

            function showActionToast(title) {
                $mdToast.show(
                    $mdToast.simple()
                        .action('OK')
                        .content(title)
                        .hideDelay(10000)
                        .position('top right')
                );
            }
        }



})();
